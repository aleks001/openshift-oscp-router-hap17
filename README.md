# what's this

This repo hold the openshift container plattform router with haproxy 1.7.

To be able to build this image you will need a OSCP subscription.


# how to build

```
oc new-app https://gitlab.com/aleks001/openshift-oscp-router-hap17.git
oc logs -f bc/openshift-oscp-router-hap17
```

wait until the push is successully.

```
oc logs -f openshift-oscp-router-hap17-1-efgqz
I0302 12:25:49.519595       1 router.go:236] Router is including routes in all namespaces
```

# various output

## ps

```
oc rsh openshift-oscp-router-hap17-1-gv48k

sh-4.2$ ps axf

   PID TTY      STAT   TIME COMMAND

    34 ?        Ss     0:00 /bin/sh

    40 ?        R+     0:00  \_ ps axf

     1 ?        Ssl    0:00 /usr/bin/openshift-router

```

## version

```
sh-4.2$ /usr/bin/openshift-router version

openshift-router v3.3.1.14

kubernetes v1.3.0+52492b4
```

## haproxy -vv 
```
sh-4.2$ haproxy -vv  
HA-Proxy version 1.7.3 2017/02/28  
Copyright 2000-2017 Willy Tarreau <willy@haproxy.org>  

Build options :  
  TARGET  = linux2628  
  CPU     = generic  
  CC      = gcc  
  CFLAGS  = -O2 -g -fno-strict-aliasing -Wdeclaration-after-statement  
  OPTIONS = USE_LINUX_SPLICE=1 USE_ZLIB=1 USE_OPENSSL=1 USE_LUA=1 USE_PCRE=1 USE_PCRE_JIT=1 USE_TFO=1  

Default settings :  
  maxconn = 2000, bufsize = 16384, maxrewrite = 1024, maxpollevents = 200  
 
Encrypted password support via crypt(3): yes  
Built with zlib version : 1.2.7  
Running on zlib version : 1.2.7  
Compression algorithms supported : identity("identity"), deflate("deflate"), raw-deflate("deflate"), gzip("gzip")  
Built with OpenSSL version : OpenSSL 1.0.1e-fips 11 Feb 2013  
Running on OpenSSL version : OpenSSL 1.0.1e-fips 11 Feb 2013  
OpenSSL library supports TLS extensions : yes  
OpenSSL library supports SNI : yes  
OpenSSL library supports prefer-server-ciphers : yes  
Built with PCRE version : 8.32 2012-11-30  
Running on PCRE version : 8.32 2012-11-30  
PCRE library supports JIT : yes  
Built with Lua version : Lua 5.3.4  
Built with transparent proxy support using: IP_TRANSPARENT IPV6_TRANSPARENT IP_FREEBIND  

Available polling systems :  
      epoll : pref=300,  test result OK  
       poll : pref=200,  test result OK  
     select : pref=150,  test result OK  
Total: 3 (3 usable), will use epoll.  

Available filters :  
        [COMP] compression  
        [TRACE] trace  
        [SPOE] spoe 
```